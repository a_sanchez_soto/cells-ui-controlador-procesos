import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-ui-controlador-procesos-styles.js';
import '@cells-components/cells-icon';
/**
This component ...

Example:

```html
<cells-ui-controlador-procesos></cells-ui-controlador-procesos>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
export class CellsUiControladorProcesos extends LitElement {
  static get is() {
    return 'cells-ui-controlador-procesos';
  }

  // Declare properties
  static get properties() {
    return {
      title: {
        type: String
      },
      items: {
        type: Array
      },
      autoPlay: {
        type: Boolean
      },
      errors: {
        type: Array
      },
      iterateErrors: {
        type: Array
      },
      map: {
        type: Object
      },
      textProcesando: {
        type: String
      },
      textEspera: {
        type: String
      },
      enableMandatory: {
        type: Boolean
      },
      description: {
        type: String
      }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.items = [];
    this.errors = [];
    this.map = {};
    this.textProcesando = 'Procesando...';
    this.textEspera = 'En espera';
    this.description = '';
    this.updateComplete.then(() => {
      if (this.autoPlay) {
        this.run();
      }
    });
  }

  addMap(key, value) {
    this.map[key] = value;
  }

  getFirstItem() {
    return new Promise((resolve, reject) => {
      let firstItem;
      let loop = setInterval(() => {
        if (this.items.length === 0) {
          clearInterval(loop);
          resolve(null);
        }
        firstItem = this.shadowRoot.getElementById('controlador-item-0');
        if (firstItem) {
          clearInterval(loop);
          resolve(firstItem);
        }
      }, 100);
    });
  }

  consolideProcess() {
    let result = { errors: this.errors, results: this.map };
    this.dispatchEvent(new CustomEvent('process-complete-event', {
      composed: true,
      bubbles: true,
      detail: result
    }));
  }

  nextStep(step) {
    let boxItem = this.shadowRoot.querySelector(`#controlador-item-${step}`);
    if (boxItem && typeof boxItem.dataset.position !== 'undefined') {
      if (this.iterateErrors && this.iterateErrors.length > 0) {
        let filterIteratorErrors = this.iterateErrors.filter((iteratorError) => {
          return iteratorError.position === parseInt(boxItem.dataset.position);
        });
        if (filterIteratorErrors.length === 0) {
          this.nextStep(step + 1);
          return;
        }
      }

      if (this.enableMandatory) {
        boxItem.querySelector('.sub-text-item').innerHTML = 'El proceso fue cancelado.';
        boxItem.querySelector('.text-item').classList.add('error');
        boxItem.querySelector('.sub-text-item').classList.add('error');
        boxItem.querySelector('.right-icon').innerHTML = '<cells-icon class = "error" icon = "coronita:error"></cells-icon>';
        this.executeStepCancel((step + 1), boxItem);
      } else {
        boxItem.querySelector('.sub-text-item').innerHTML = this.textProcesando;
        boxItem.querySelector('.right-icon').innerHTML = '<div class="lds-css ng-scope"><div class="lds-rolling"><div  ></div></div>';
        this.items[step].execute(boxItem, this, (step + 1));
      }

    }
  }

  executeStepCancel(step, boxItem) {
    let positionExecute = parseInt(boxItem.dataset.position);
    this.errors.push({
      name: this.items[positionExecute].name || 'undefined',
      item: this.items[positionExecute],
      position: positionExecute
    });
    this.nextStep(step);
    if (step === this.items.length) {
      this.consolideProcess();
    }
  }

  finalizeStep(options) {
    if (options.item) {
      let box = options.item;
      let contentText = box.querySelector('.content-text');
      let positionExecute = parseInt(box.dataset.position);
      contentText.querySelector('.text-item').innerHTML = options.message;
      if (!this.enableMandatory) {
        contentText.querySelector('.sub-text-item').innerHTML = options.bottomMessage || '';
      }
      if (options.success) {
        contentText.querySelector('.text-item').classList.remove('error');
        contentText.querySelector('.sub-text-item').classList.remove('error');
        box.querySelector('.right-icon').innerHTML = '<cells-icon icon = "coronita:correct"></cells-icon>';
      } else {

        this.errors.push({
          name: this.items[positionExecute].name || 'undefined',
          item: this.items[positionExecute],
          position: positionExecute,
          message: options.message,
          bottomMessage: options.bottomMessage
        });

        contentText.querySelector('.text-item').classList.add('error');
        contentText.querySelector('.sub-text-item').classList.add('error');
        box.querySelector('.right-icon').innerHTML = '<cells-icon class = "error" icon = "coronita:error"></cells-icon>';
        this.enableMandatory = this.items[positionExecute].mandatory;
      }
      if (this.items.length === (positionExecute + 1)) {
        this.consolideProcess();
      }
    }
  }

  async run() {
    this.errors = [];
    this.iterateErrors = [];
    this.enableMandatory = false;
    let firstItem = await this.getFirstItem();
    if (firstItem) {
      firstItem.querySelector('.right-icon').innerHTML = '<div class="lds-css ng-scope"><div class="lds-rolling"><div  ></div></div>';
      firstItem.querySelector('.sub-text-item').innerHTML = this.textProcesando;
      if (typeof firstItem.dataset.position !== 'undefined') {
        let step = parseInt(firstItem.dataset.position);
        this.items[step].execute(firstItem, this, (step + 1));
      }
    }
  }

  reintent() {
    this.iterateErrors = [];
    if (this.errors.length > 0) {
      this.enableMandatory = false;
      this.iterateErrors = this.errors;
      this.errors = [];
      this.nextStep(this.iterateErrors[0].position);
    }
  }

  executeReintent() {
    if (this.iterateErrors && this.iterateErrors.length > 0) {

    }
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-ui-controlador-procesos-shared-styles').cssText}
    `;
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
      <div class="box-container-controller"  >
        <div ?hidden = ${!this.title} class = "header-controller">${this.title}</div>
        <div class="body-controller" >
          <h2>${this.description}</h2>
          ${this.items.map((item, index) => html`
            <div data-position = "${index}" id = "controlador-item-${index}" class="box-process" >
              <div class = "vineta">
                <cells-icon icon = "coronita:forward"></cells-icon>
              </div>
              <div class = "content-text" >
                <span class = "text-item">${item.text}</span>
                <br>
                <span class = "sub-text-item">${this.textEspera}</span>
              </div>
              <div class="right-icon" >
              
              </div>
            </div>
          `)}
        </div>
      </div>
    `;
  }
}

// Register the element with the browser
customElements.define(CellsUiControladorProcesos.is, CellsUiControladorProcesos);
