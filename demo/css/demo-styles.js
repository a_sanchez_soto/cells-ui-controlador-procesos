import { setDocumentCustomStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import { css, } from 'lit-element';

setDocumentCustomStyles(css`
  #iframeBody {
    margin: 0;
    display: flex;
    justify-content: center;
    margin:15px;
  }

  .demo-container {
    width:650px;
  }

  .button {
    color: #fff;
    background-color: #004481;
    border: none;
    display: inline-block;
    padding: 10px 20px;
    vertical-align: middle;
    overflow: hidden;
    text-decoration: none;
    text-align: center;
    cursor: pointer;
    white-space: nowrap;
  }

  .button:disabled,
  .button[disabled] {
    opacity:0.6;
  }

  .content-button {
    text-align: center;
    margin-bottom:10px;
  }

  .hide {
    display: none;
  }

`);
