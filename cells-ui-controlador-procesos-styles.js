import { css, } from 'lit-element';


export default css`:host {
  display: block;
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  margin: 0;
  padding: 0;
  @apply --cells-ui-controlador-procesos; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit; }

.box-container-controller {
  width: 100%;
  border: 1px solid #D3D3D3; }

.header-controller {
  width: 100%;
  text-align: center;
  padding: 10px;
  background-color: #072146;
  color: #fff;
  text-transform: uppercase;
  font-size: 0.9rem;
  text-shadow: 1px 1px 1px #000; }

h2 {
  font-weight: normal;
  color: #333;
  font-size: 0.9rem;
  padding: 0;
  margin: 0;
  margin-bottom: 10px; }

.body-controller {
  width: 100%;
  padding: 15px;
  color: #333;
  font-size: 0.85rem; }

.box-process {
  width: 100%;
  padding: 10px 5px;
  margin-bottom: 10px;
  background-color: #F4F4F4;
  color: #333;
  border: 1px solid #D3D3D3;
  display: flex;
  align-items: stretch; }

.vineta {
  width: 22px;
  margin-top: -4px; }

.content-text {
  width: calc(100% - 52px);
  font-size: 0.82rem;
  line-height: 1rem; }

.right-icon {
  width: 30px;
  margin-top: 4px;
  position: relative;
  color: #388D4F; }

.error {
  color: #CB353A !important; }

.sub-text-item {
  font-size: 0.7rem;
  color: #666; }

@keyframes lds-rolling {
  0% {
    -webkit-transform: translate(-50%, -50%) rotate(0deg);
    transform: translate(-50%, -50%) rotate(0deg); }
  100% {
    -webkit-transform: translate(-50%, -50%) rotate(360deg);
    transform: translate(-50%, -50%) rotate(360deg); } }

@-webkit-keyframes lds-rolling {
  0% {
    -webkit-transform: translate(-50%, -50%) rotate(0deg);
    transform: translate(-50%, -50%) rotate(0deg); }
  100% {
    -webkit-transform: translate(-50%, -50%) rotate(360deg);
    transform: translate(-50%, -50%) rotate(360deg); } }

.lds-rolling {
  position: absolute;
  z-index: 5;
  top: -7px;
  right: 5px; }

.lds-rolling div,
.lds-rolling div:after {
  position: absolute;
  width: 120px;
  height: 120px;
  border: 10px solid #9f9f9f;
  border-top-color: transparent;
  border-radius: 50%; }

.lds-rolling div {
  -webkit-animation: lds-rolling 1s linear infinite;
  animation: lds-rolling 1s linear infinite;
  top: 130px;
  left: 85px; }

.lds-rolling div:after {
  -webkit-transform: rotate(90deg);
  transform: rotate(90deg); }

.lds-rolling {
  width: 25px !important;
  height: 25px !important;
  -webkit-transform: translate(-20px, -20px) scale(0.2) translate(20px, 20px);
  transform: translate(-20px, -20px) scale(0.2) translate(20px, 20px); }
`;